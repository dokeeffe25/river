(ns river.producer
  (:require [grete.producer :as prod]
            [mount.core :as mount]
            [river.config :as config]))

(mount/defstate ^{:on-reload :noop}
  producer
  :start
  (when-let [prod-config (:producer config/env)]
    (prod/producer prod-config))
  :stop
  (when producer
    (prod/close! producer)))
