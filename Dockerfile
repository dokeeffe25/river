FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/river.jar /river/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/river/app.jar"]
