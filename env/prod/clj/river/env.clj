(ns river.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[river started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[river has shut down successfully]=-"))
   :middleware identity})
