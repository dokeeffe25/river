(ns ^:figwheel-no-load river.app
  (:require [river.core :as core]
            [devtools.core :as devtools]))

(enable-console-print!)

(devtools/install!)

(core/init!)
