(ns user
  (:require [mount.core :as mount]
            [river.figwheel :refer [start-fw stop-fw cljs]]
            river.core))

(defn start []
  (mount/start-without #'river.core/repl-server))

(defn stop []
  (mount/stop-except #'river.core/repl-server))

(defn restart []
  (stop)
  (start))


