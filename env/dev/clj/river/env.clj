(ns river.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [river.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[river started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[river has shut down successfully]=-"))
   :middleware wrap-dev})
