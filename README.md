# River Chat

generated using Luminus version "2.9.11.53"

Realtime chat app using Kafka and Clojure

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein run

## License

Copyright © 2017 David O'Keeffe
